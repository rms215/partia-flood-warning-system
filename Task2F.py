from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.analysis import polyfit
import datetime
from floodsystem.plot import plot_water_level_with_fit
stations = build_station_list()
stations_list = []
level_list = []

update_water_levels(stations)

for station in stations:
    stations_list.append(station)

for i in range (len(stations_list)):
    difference = stations_list[i].relative_water_level()
    if difference != None:
        level_list.append((stations_list[i],difference))
listStations= sorted(level_list, key=lambda x: x[1])
listStations.reverse()
top5 = []

for i in range(5):
     top5.append(listStations[i][0])
     dt = 2
     p = 4
     dates, levels = fetch_measure_levels(top5[i].measure_id, dt=datetime.timedelta(days=dt))
     plot_water_level_with_fit(top5[i], dates, levels, p)

     