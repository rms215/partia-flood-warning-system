from floodsystem.geo import rivers_with_station, stations_by_river
from floodsystem.stationdata import build_station_list

stations = build_station_list()

def run():
    allStations = rivers_with_station(stations)
    print(len(allStations))
    allStations.sort()
    print(allStations[:10])

    riversDictionary = stations_by_river(stations)
    print(riversDictionary["River Aire"])
    print(riversDictionary["River Cam"])
    print(riversDictionary["River Thames"])

if __name__ == "__main__":
    run()