from floodsystem.geo import stations_in_radius
from floodsystem.stationdata import build_station_list

stations = build_station_list()

def run():
    closeStations = stations_in_radius(stations,(52.2053, 0.1218),10.0)
    print(closeStations)

if __name__ == "__main__":
    run()