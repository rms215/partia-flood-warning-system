from floodsystem.flood import stations_level_over_threshold
from floodsystem.stationdata import build_station_list, update_water_levels

stations = build_station_list()
update_water_levels(stations)

def run():
    for station in stations_level_over_threshold(stations, 0.8): 
        print(station[0], station[1])
if __name__ == "__main__":
    run()