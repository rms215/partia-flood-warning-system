from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations
stations = build_station_list()

def run():
    print(inconsistent_typical_range_stations(stations))
    
if __name__ == "__main__":
    run()