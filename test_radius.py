#Unit test for finding stations within a given radius

from floodsystem.geo import stations_in_radius
from floodsystem.stationdata import build_station_list

def test_radius():
    stations = build_station_list()
    closeStations = stations_in_radius(stations,(52.2053, 0.1218),10.0)
    if "Girton" in closeStations:
        girton = True

    assert girton