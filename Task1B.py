from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_distance

stations = build_station_list()

def run():
    allStations = stations_by_distance(stations,(52.2053, 0.1218))

    closest = allStations[:10]
    furthest = allStations[-10:]

    print(closest)
    print(furthest)

if __name__ == "__main__":
    run()