##Unit test for Distance between coordinates

from floodsystem.geo import distanceBetweenCo

def test_distance():
    a = (23.5,67.5)
    b = (25.5,69.5)
    distance = distanceBetweenCo(a,b)

    assert round(distance,2) == 300.67