### Check 'River by Station Number' Funtion

from floodsystem.geo import rivers_by_station_number
from floodsystem.stationdata import build_station_list
    
def test_station_number():
    stations = build_station_list()
    a = rivers_by_station_number(stations, 1)

    assert a[0][0] == 'River Thames'