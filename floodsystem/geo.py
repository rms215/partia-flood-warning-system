# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""

from .utils import sorted_by_key  # noqa

#Given a list of station objects and co-ordinate p
#Return a list of (station,distance) tuples  
#Distance(float) is the distance of station(moniteringStation) from p
#Return list sorted by distance


####This code finds distance between to co-ordinates
def distanceBetweenCo(a,b):
    from math import sin, cos, sqrt, atan2, radians

    # approximate radius of earth in km
    R = 6371.0

    lat1 = radians(a[0]) #Seperate co-ordinates out into lat and long
    lon1 = radians(a[1])
    lat2 = radians(b[0])
    lon2 = radians(b[1])

    dlon = lon2 - lon1 #Difference between lat and long
    dlat = lat2 - lat1

    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2 #Formula to find distance
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    distance = R * c

    return distance


###Task 1B
def stations_by_distance(stations,p):
    listStations = []

    for station in stations:
        distance = distanceBetweenCo(station.coord,p)
        listStations.append((station.name,station.town,distance))
    listStations= sorted(listStations, key=lambda x: x[2])
    return listStations


###Task 1C
def stations_in_radius(stations,centre,r):
    listStations = []
    for station in stations:
        distance = distanceBetweenCo(station.coord,centre)
        if distance <= r:
            listStations. append(station.name)
    listStations.sort()
    return listStations


###Task 1D
def rivers_with_station(stations):
    listRivers = []
    for station in stations:
        river = station.river
        if river not in listRivers: 
            listRivers.append(river)
    return listRivers

def stations_by_river(stations):
    riversList = rivers_with_station(stations)
    riversDictionary = {}
    for river in riversList:
        listStations = []
        for station in stations:
            if station.river == river:
                listStations.append(station.name)
        listStations.sort()
        riversDictionary[river] = listStations
    return riversDictionary


###Task 1E
def rivers_by_station_number(stations, N):
    riversList = rivers_with_station(stations)
    rivers = []
    for river in riversList:
         a = 0
         for station in stations:
            if station.river == river:
                 a += 1
         rivers.append((river, a))
    rivers = sorted_by_key(rivers, 1, True)

    riverdata = []
    A = rivers[N-1][1]
    for river in rivers:
        if river[1] >= A:
            riverdata.append(river)
    riverdata = sorted_by_key(riverdata, 1, True)
    return riverdata

