import matplotlib.dates
import numpy as np
import matplotlib.pyplot as plt

def polyfit(dates, levels, p):
    x = matplotlib.dates.date2num(dates)
    y = x[0]
    x = [a - y for a in x]

    p_coeff = np.polyfit(x, levels, p)
    return p_coeff, y


