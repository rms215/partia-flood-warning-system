from .datafetcher import fetch_measure_levels
import datetime

def find_stations_at_risk(stations_list):
    low = []
    moderate = []
    high = []
    severe = []
    for x in range(len(stations_list)):
        print(x)
        try:
            dates, levels = fetch_measure_levels(stations_list[x].measure_id, dt=datetime.timedelta(days=1))
            levels2 = levels[-10:]
            ####Low if always withing usual range, or below usual range
            dontappend = False
            for value in levels:
                if value > stations_list[x].typical_range[1]:
                    dontappend = True
            if dontappend == False:
                low.append(stations_list[x].name)


            ####Severe if permantly increasing for last 10 values
            ####High if above 0.5
            ###Moderare otherwise

            elif all(x<y for x, y in zip(levels2, levels2[1:])):
                if levels[-1] - stations_list[x].typical_range[1] > 1:
                    severe.append(stations_list[x].name)
                if levels[-1] - stations_list[x].typical_range[1] > 0.5:
                    high.append(stations_list[x].name)
                else:
                    moderate.append(stations_list[x].name)


            ####Moderate if permantly decreasing for last 10 values and more than 0.75 above range
            ####Low if permantly decreasing for last 10 values and not more than 0.75 above range
            elif all(x>y for x, y in zip(levels2, levels2[1:])):
                if levels[-1] - stations_list[x].typical_range[1] > 0.75:
                    moderate.append(stations_list[x].name)
                else:
                    low.append(stations_list[x].name)

            #####High if neither all increasing or all decreasing, but above 0.75
            #####Moderate if neither all increasing or all decreasing, but above 0.25
            ####Otherwise low

            elif not all(x>y for x, y in zip(levels2, levels2[1:])) and not all(x<y for x, y in zip(levels2, levels2[1:])):
                if levels[-1] - stations_list[x].typical_range[1] > 0.75:
                    high.append(stations_list[x].name)
                elif levels[-1] - stations_list[x].typical_range[1] > 0.25:
                    moderate.append(stations_list[x].name) 
                else:
                    low.append(stations_list[x].name) 

        except:
            pass

    print("Severe risk:",severe)
    print("High risk:",high)  