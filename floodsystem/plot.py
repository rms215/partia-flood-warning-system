import matplotlib.dates
import numpy as np

import datetime
import matplotlib.pyplot as plt
from .analysis import polyfit



def plot_water_levels(station, dates, levels):
    low = []
    high = []
    for i in range (len(dates)):
        low.append(station.typical_range[0])
        high.append(station.typical_range[1])

    # Plot
    plt.plot(dates, levels)
    plt.plot(dates,low)
    plt.plot(dates,high)

    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45);
    plt.title(station.name)

    # Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels

    plt.show()

def plot_water_level_with_fit(station, dates, levels, p):
    p_coeff, d0 = polyfit(dates, levels, p)
    x = matplotlib.dates.date2num(dates)
    x = [a - d0 for a in x]
    number = 0
    for value in levels:
        number += 1

    x1 = np.linspace(x[0], x[-1], number)
    poly = np.poly1d(p_coeff)
    low = []
    high = []
    for i in range (len(dates)):
        low.append(station.typical_range[0])
        high.append(station.typical_range[1])

    plt.plot(x1,low)
    plt.plot(x1,high)
    plt.plot(x1, poly(x1))
    plt.plot(x1, levels)

    plt.title(station.name)

    plt.show()

