from .utils import sorted_by_key

###Task 2B
def stations_level_over_threshold(stations, tol):
    flood_stations = []
    for station in stations:
        if station.relative_water_level() != None:
            if station.relative_water_level() > tol:
                flood_stations.append((station.name, station.relative_water_level()))
    return sorted_by_key(flood_stations, 1, True)


###Task 2C
def stations_highest_rel_level(stations, N):
    stationsdata = []
    for station in stations:
        if station.relative_water_level() != None:
            stationsdata.append((station.name, station.relative_water_level()))
    stationsdata = sorted_by_key(stationsdata, 1, True)
    return stationsdata[:N]