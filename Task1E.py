from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_by_station_number, rivers_with_station
stations = build_station_list()

def run():
    rivers_with_lots_of_stations = rivers_by_station_number(stations, 9)
    print(rivers_with_lots_of_stations)
    
if __name__ == "__main__":
    run()