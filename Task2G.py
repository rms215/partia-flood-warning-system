from floodsystem.at_risk import find_stations_at_risk
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.stationdata import build_station_list, update_water_levels
import datetime
import numpy as np
from floodsystem.plot import plot_water_level_with_fit

stations = build_station_list()
stations_list = []
level_list = []

update_water_levels(stations)

for station in stations:
    stations_list.append(station)


find_stations_at_risk(stations_list)

