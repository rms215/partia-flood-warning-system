###Test closest distance function
from floodsystem.geo import stations_by_distance

from floodsystem.stationdata import build_station_list

def test_closest():
    stations = build_station_list()
    allStations = stations_by_distance(stations,(52.2053, 0.1218))
    closest = allStations[0]

    assert closest == ('Cambridge Jesus Lock', 'Cambridge', 0.8402364350834995)